Algoritmo Ejercicio8
	//8.Realizar un diagrama de flujo que lea N numeros, calcule y escriba la suma de los pares y el producto de los impares.
	//Producto se refiere a multiplicacion, es decir la multiplicacion de los impares
	leer cant
	producto = 1
	
	Repetir
		num = num + 1
		
		escribir num
		si num mod 2 = 0 entonces
			suma = suma + num
		FinSi
		
		si num mod 2 > 0 entonces
			producto = producto * num
		FinSi
		
	Hasta Que num = cant
	
	escribir "la suma de los numeros pares es " suma
	escribir "el producto de los numeros impares es " producto
FinAlgoritmo
