Algoritmo Ejercicio9
	//9.Hace un diagrama de flujo para calcular el maximo comun divisor de dos numeros enteros positivos N y M siguiendo el algoritmo de Euclides, que es el siguiente:
	//a)Se divide N por M, sea R el resto.
	//b)Si R=0, el maximo comun divisor es M y se acaba.
	//c)Se asigna a N el valor de M y a M el valor de R y volver al paso a.
	
	escribir "ingrese el valor de N"
	leer n
	escribir "ingrese el valor de M"
	leer m
	
	Repetir
		r = n mod m
		
		si r = 0 Entonces
			escribir "maximo comun divisor es " m
			valido = 1
		SiNo
			n = m
			m = r
		FinSi
	Hasta Que valido = 1
	
	//al final solo interprete los pasos de a, b y c. en resumen ellos dicen como sacar este resultado
FinAlgoritmo
