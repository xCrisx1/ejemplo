Algoritmo estructurasi
	//Algoritmo que lea dos numeros y nos diga cual de ellos es mayor o bien si son iguales (recuerda usar la estructura condicional si)
	
	escribir "Indique el primer valor"
	leer val1
	
	escribir "Indique el segundo valor"
	leer val2
	
	si val1 > val2 Entonces
		escribir val1 " es mayor que " val2
	FinSi
	
	si val2 > val1 Entonces
		escribir val2 " es mayor que " val1
	FinSi
	
	si val1 = val2 Entonces
		escribir val2 " es igual que " val1
	FinSi
	
	////otra forma
	escribir "siguiente forma mostrada a partir de aqui"
	
	si val1 >= val2 Entonces
		si val1 = val2 entonces
			escribir val1 " es igual que " val2
		SiNo
			escribir val1 " es mayor que " val2
		FinSi
	SiNo
		escribir val2 " es mayor que " val1
	FinSi
FinAlgoritmo
