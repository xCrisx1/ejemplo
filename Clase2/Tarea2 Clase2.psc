Algoritmo Tarea2
	//ALGORITMO QUE LEA TRES NUMEROS DISTINTOS Y NOS DIGA CUAL DE ELLOS ES EL MAYOR (RECORDAR USAR ESTRUCTURA CONDICIONAL SI Y LOS OPERADORES LOGICOS)
	
	escribir "introduzca el primer valor"
	leer val1
	
	escribir "introduzca el segundo valor"
	leer val2
	
	escribir "introduzca el tercer valor"
	leer val3
	
	si val1 > val2 Entonces
		//val 1 es mayor que 2
		si val1 > val3 Entonces
			escribir val1 " es mayor que " val2 " y " val3
		SiNo
			si val3 > val2 Entonces
				escribir val1 " es mayor que " val2 " y " val3
			SiNo
				escribir val2 " es mayor que " val1 " y " val3
			FinSi
		FinSi
	SiNo
		si val2 > val3 Entonces
			escribir val2 " es mayor que " val1 " y " val3
		SiNo
			escribir val3 " es mayor que " val1 " y " val2
		FinSi
	FinSi
	
FinAlgoritmo
