Algoritmo Ejercicio5Forma2
	//5. hacer un diagrama de flujo para sumar los N primeros numeros impares.
	//Realizar luego uno que haga lo mismo pero con los numeros pares, otro con los multiplos de 3 y otro con los multiplos de 5
	
	//numeros impares:
	escribir "Ingrese la cantidad de los primeros numeros impares a sumar"
	leer cantidad
	
	x = 0
	n = 0
	
	escribir "los primeros " cantidad " numeros impares son:"
	Mientras n < cantidad Hacer
		
		x = x + 1
		si x mod 2 > 0 entonces
			escribir x
			n = n + 1
			suma = suma + x
		FinSi
	Fin Mientras
	escribir "la suma de los numeros impares es de " suma
	
	//numeros pares:
	escribir "Ingrese la cantidad de los primeros numeros pares a sumar"
	leer cantidad
	
	x = 0
	n = 0
	
	escribir "los primeros " cantidad " numeros pares son:"
	Mientras n < cantidad Hacer
		
		x = x + 1
		si x mod 2 = 0 entonces
			escribir x
			n = n + 1
			suma = suma + x
		FinSi
	Fin Mientras
	escribir "la suma de los numeros pares es de " suma
	
	//numeros multiplos de 3:
	escribir "Ingrese la cantidad de los primeros numeros multiplos de 3 a sumar"
	leer cantidad
	
	x = 0
	n = 0
	
	escribir "los primeros " cantidad " numeros multiplos de 3 son:"
	Mientras n < cantidad Hacer
		
		x = x + 1
		si x mod 3 = 0 entonces
			escribir x
			n = n + 1
			suma = suma + x
		FinSi
	Fin Mientras
	escribir "la suma de los numeros multiplos de 3 es de " suma
	
	//numeros multiplos de 5:
	escribir "Ingrese la cantidad de los primeros numeros multiplos de 5 a sumar"
	leer cantidad
	
	x = 0
	n = 0
	
	escribir "los primeros " cantidad " numeros multiplos de 5 son:"
	Mientras n < cantidad Hacer
		
		x = x + 1
		si x mod 5 = 0 entonces
			escribir x
			n = n + 1
			suma = suma + x
		FinSi
	Fin Mientras
	escribir "la suma de los numeros multiplos de 5 es de " suma
	
FinAlgoritmo
