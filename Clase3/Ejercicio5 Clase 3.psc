Funcion numeropar <- esnumeropar ( valor1 )
	i = 0
	Mientras valor1 > i Hacer
		i = i + 2
	Fin Mientras
	
	si i = valor1 Entonces
		numeropar = 1
	SiNo
		numeropar = 0
	FinSi
	
	si i = 0 entonces
		numeropar = 0
	FinSi
Fin Funcion

Funcion numeroimpar <- esnumeroimpar ( valor1 )
	i = 1
	Mientras valor1 > i Hacer
		i = i + 2
	Fin Mientras
	
	si i = valor1 Entonces
		numeroimpar = 1
	SiNo
		numeroimpar = 0
	FinSi
	
	si i = 0 entonces
		numeroimpar = 0
	FinSi
Fin Funcion

Funcion numeromultiplo3 <- esnumeromultiplo3 ( valor1 )
	i = 0
	Mientras valor1 > i Hacer
		i = i + 3
	Fin Mientras
	
	si i = valor1 Entonces
		numeromultiplo3 = 1
	SiNo
		numeromultiplo3 = 0
	FinSi
	
	si i = 0 entonces
		numeromultiplo3 = 0
	FinSi
Fin Funcion

Funcion numeromultiplo5 <- esnumeromultiplo5 ( valor1 )
	i = 0
	Mientras valor1 > i Hacer
		i = i + 5
	Fin Mientras
	
	si i = valor1 Entonces
		numeromultiplo5 = 1
	SiNo
		numeromultiplo5 = 0
	FinSi
	
	si i = 0 entonces
		numeromultiplo5 = 0
	FinSi
Fin Funcion


Algoritmo Ejercicio5
	//5. hacer un diagrama de flujo para sumar los N primeros numeros impares.
	//Realizar luego uno que haga lo mismo pero con los numeros pares, otro con los multiplos de 3 y otro con los multiplos de 5
	
	escribir "Ingrese la cantidad de numeros impares a calcular"
	leer n
	i = 1
	x = 0
	suma = 0
	
	escribir "los primeros " n " numeros impares son:"
	
	Mientras x < n Hacer
		saber = esnumeroimpar(i)
		
		si saber = 1 entonces
			x = x + 1
			suma = suma + i
			escribir i
		FinSi
		
		i = i + 1
	Fin Mientras
	escribir "el resultado de la suma de " n " primeros numeros impares a calcular es de " suma
	
	escribir "Ingrese la cantidad de numeros pares a calcular"
	leer n
	i = 1
	x = 0
	suma = 0
	
	escribir "los primeros " n " numeros pares son:"
	Mientras x < n Hacer
		saber = esnumeropar(i)
		
		si saber = 1 entonces
			x = x + 1
			suma = suma + i
			escribir i
		FinSi
		
		i = i + 1
	Fin Mientras
	escribir "el resultado de la suma de " n " primeros numeros pares a calcular es de " suma
	
	escribir "Ingrese la cantidad de numeros multiplos de 3 a calcular"
	leer n
	i = 1
	x = 0
	suma = 0
	
	escribir "los primeros " n " numeros multiplos de 3 son:"
	Mientras x < n Hacer
		saber = esnumeromultiplo3(i)
		
		si saber = 1 entonces
			x = x + 1
			suma = suma + i
			escribir i
		FinSi
		
		i = i + 1
	Fin Mientras
	escribir "el resultado de la suma de " n " primeros numeros multiplo de 3 a calcular es de " suma
	
	escribir "Ingrese la cantidad de numeros multiplo de 5 a calcular"
	leer n
	i = 1
	x = 0
	suma = 0
	
	escribir "los primeros " n " numeros multiplos de 5 son:"
	Mientras x < n Hacer
		saber = esnumeromultiplo5(i)
		
		si saber = 1 entonces
			x = x + 1
			suma = suma + i
			escribir i
		FinSi
		
		i = i + 1
	Fin Mientras
	escribir "el resultado de la suma de " n " primeros numeros multiplos de 5 a calcular es de " suma
FinAlgoritmo
