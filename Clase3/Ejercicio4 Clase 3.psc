Funcion numeropar <- esnumeropar ( valor1 )
	i = 0
	Mientras valor1 > i Hacer
		i = i + 2
	Fin Mientras
	
	si i = valor1 Entonces
		numeropar = 1
	SiNo
		numeropar = 0
	FinSi
Fin Funcion

Algoritmo Ejercicio4
	//4. Hacer un diagrama de flujo que permita escribir los 100 primeros numeros pares.
	x = 0
	n = 0
	Mientras n <= 100 Hacer
		
		x = x + 1
		saber = esnumeropar(x)
		
		si saber = 1 entonces
			escribir x
			n = n + 1
		FinSi
		
	Fin Mientras
FinAlgoritmo
