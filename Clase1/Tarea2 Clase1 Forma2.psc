Algoritmo Tarea2Forma2
	
	escribir "Escriba que tarea desea realizar (*,/,+,-)"
	leer tipo
	
	escribir "Escriba el primer valor"
	leer numero1
	
	escribir "Escriba el segundo valor"
	leer numero2
	
	si tipo = "*" entonces
		resultado = numero1 * numero2
		escribir "el resultado de: " numero1 "*" numero2 "=" resultado		
	FinSi
	
	si tipo = "+" entonces
		resultado = numero1 + numero2
		escribir "el resultado de: " numero1 "+" numero2 "=" resultado		
	FinSi
	
	si tipo = "-" Entonces
		resultado = numero1 - numero2
		escribir "el resultado de: " numero1 "-" numero2 "=" resultado		
	FinSi
	
	si tipo = "/" Entonces
		resultado = numero1 / numero2
		escribir "el resultado de: " numero1 "/" numero2 "=" resultado		
	FinSi
	
FinAlgoritmo
