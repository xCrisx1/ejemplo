Algoritmo EjercicioPrueba
	//Se requiere determinar el las imposiciones de un trabajador con base en las horas que trabaja y el pago por hora que recibe. 
	//Tomando en cuenta que las imposiciones de este trabajador estan conformadas por un 10% destinado al pago de AFP y un 7% al pago de Salud.
	//Realice el diagrama de flujo y el pseudocódigo que representen el algoritmo de solución correspondiente.
	
	escribir "ingresa el pago por hora que recibes"
	leer pago
	
	escribir "ingresa cuantas horas has trabajado"
	leer horas
	
	total = pago * horas
	
	afp = total * 0.10
	
	salud = total * 0.07
	
	imposiciones = afp + salud
	
	escribir "su sueldo es de $" total
	escribir "sus imposiciones son de $" imposiciones
	escribir "su sueldo quedaria en $" total - imposiciones
	escribir "su sueldo por hora sin imposiciones es de $" (pago - (pago * 0.17))
FinAlgoritmo
