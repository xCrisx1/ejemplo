Algoritmo Ejercicio6
	//La sucesion fibonacci se define de la sigueinte forma a(1)=1, a(2)=1 y a(n)=a(n+1)+a(n-2) para n>2, es decir los dos primeros son 1 y el resto cada uno es la suma 
	//de los dos anteriores, los primeros numeros de la sucesion son: 1,1,2,3,5,8,13,21,... Hacer un diagrama de flujo para calcular n-esimo termino de la sucesion.
	escribir "defina la cantidad (para que no sea infinito)"
	leer cant
	a = 1
	b = 0
	c = 0
	
	Mientras cont < cant Hacer
		c = a + b
		a = b
		b = c
		cont = cont + 1
		escribir c
	Fin Mientras
FinAlgoritmo
