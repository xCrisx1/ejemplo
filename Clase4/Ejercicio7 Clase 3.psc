Algoritmo Ejercicio7
	//Hacer un diagrama de flujo que simule un reloj.
	seg = 0
	min = 0
	hor = 0
	
	Mientras Verdadero Hacer
		seg = seg + 1
		
		si seg >= 60 entonces
			min = min + 1
			seg = 0
		FinSi
		
		si min >= 60 entonces
			hor = hor + 1
			min = 0
		FinSi
		
		si hor >= 24 entonces
			hor = 0
		FinSi
		
		Borrar Pantalla
		escribir hor ":" min ":" seg
		
		esperar 1 Segundos
		
	Fin Mientras
FinAlgoritmo
