Algoritmo Ejercicio15
	//15. Realice un diagrama de flujo que realice lo siguiente:
	//Solicite el ingreso de un numero hasta que se ingrese el valor cero. Luego al numero ingresado se le debe realizar las siguientes operaciones hasta convertirse
	//en el valor 1.
	//Si el numero es par debe dividir por 2, sino se debe multiplicar por 3 y sumarle 1.
	//Una vez ingresado el valor cero, se debe mostrar la cantidad de numeros ingresados y la cantidad de pasos realizados para convertirlos en 1.
	
	escribir "Ingrese la cantidad de numeros a ingresar"
	leer maximo
	escribir ""
	
	Dimension a[maximo]
	dimension espar[maximo]
	dimension final[maximo]
	Repetir
		escribir "ingrese un valor, cuando quiera dejar de ingresar valores, debe ingresar un 0"
		leer val1
		
		si val1 > 0 entonces
			a[cont] = val1
			
			si val1 mod 2 = 0 Entonces
				final[cont] = a[cont] / 2
				Mientras redon(final[cont]) <> 1 Hacer
					final[cont] = final[cont] / 2
					espar[cont] = espar[cont] + 1
				Fin Mientras
			SiNo
				final[cont] = a[cont] * 3 + 1
				espar[cont] = 0
			FinSi
		FinSi
		
		cont = cont + 1
	Hasta Que val1 = 0 o cont = maximo
	
	escribir "se han ingresado " cont - 1 " valores, los cuales son:"
	Repetir
		escribir a[cont2] " el cual finalmente dio de valor " final[cont2]
		si espar[cont2] = 0 entonces
			escribir "para sacar este resultado se debio multiplicar " a[cont2] " por 3 y sumarle + 1, ya que es un numero impar" 
		SiNo
			escribir "para sacar este resultado se debio dividir " a[cont2] " por 2 (" espar[cont2] " veces) hasta dar 1, ya que es un numero par"
		FinSi
		escribir ""
		cont2 = cont2 + 1
	Hasta Que cont2 = (cont - 1)
	
FinAlgoritmo
