Algoritmo Ejercicio14
	//14. Utilizando la notacion de diagrama de flujo dise�e un algoritmo para determinar si un numero n es magico o no
	//Un numero se dice magico cuando la suma de sus divisores es igual al numero. En la suma no se debe considerar al mismo numero como un divisor.
	//Ejemplo:
	//28 = 1+2+4+7+14 es un numero magico
	//32!= 1+2+4+8+16 no es un numero magico
	escribir "ingrese un numero para saber si es magico o no"
	leer n
	
	Para i<-1 Hasta n - 1 Con Paso 1 Hacer
		
		si n mod i = 0 Entonces
			suma = suma + i
		FinSi
	Fin Para
	
	si suma = n Entonces
		escribir "es un numero magico"
	SiNo
		escribir "no es un numero magico"
	FinSi
FinAlgoritmo
