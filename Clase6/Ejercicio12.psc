Algoritmo Ejercicio12
	//Dados los numeros enteros positivos N y D, se dice que D es un divisor de N si el resto de dividir N entre D es 0. Se dice que un numero N es perfecto si la suma de sus divisores
	//(excluido el propio N) es N. Por ejemplo 28 es perfecto pues sus divisores (excluido el 28) son: 1,2,4,7 y 14 y su suma es 1+2+3+4+7+14=28, realizar un diagrama de flujo que
	//determine dado un numero N si este es o no perfecto.
	escribir "ingrese el valor de N"
	leer n
	
	x = 0
	
	Para d<-1 Hasta n - 1 Con Paso 1 Hacer
		r = n mod d
		
		si r = 0 entonces
			x = x + d
			escribir d
		FinSi
		
	Fin Para
	
	si x = n Entonces
		escribir n " es un numero perfecto"
	SiNo
		escribir n " es un numero imperfecto"
	FinSi
FinAlgoritmo
