Algoritmo Ejercicio13
	// 13.Un a�o es bisiesto si es multiplo de 4, exceptuando los multiplos de 100, que solo son bisiestos cuando son multiplos ademas de 400, por ejemplo el a�o 1990 no fue bisiesto,
	// pero el a�o 2000 si lo fue. hacer un diagrama de flujo que dado un a�o A nos diga si es bisiesto
	Escribir 'Ingrese un a�o'
	Leer a
	Si a MOD 400=0 Entonces
		Escribir 'es bisiesto'
	SiNo
		Si a MOD 4=0 Y a MOD 100<>0 Entonces
			Escribir 'es bisiesto'
		SiNo
			Escribir 'no es bisiesto'
		FinSi
	FinSi
FinAlgoritmo

