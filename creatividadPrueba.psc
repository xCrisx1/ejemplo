Algoritmo Adivinanzas	//Adivinanzas tipo juego�?�?
	
	Repetir
		Borrar Pantalla
		Escribir "Debes adivinar en lo que estoy pensando... para ello comenzare dando pistas"
		Escribir "Si adivinas ganar�s, de lo contrario perderas y te ahogaras en tu fracaso xD"
		Escribir "(Recuerda que es 1 palabra y solo tienes 3 intentos con 3 pistas diferentes)"
		
		adivinanza = azar(3)
		ganador = 0
		
		Segun adivinanza
			0:
				Escribir "Es un objeto rojo y las personas la consumen �que es?"
				leer palabra
				
				si palabra = Minusculas("manzana") entonces
					ganador = 1
				FinSi
			1:
				Escribir "Se utiliza para transportar personas y es muy veloz �que es?"
				leer palabra
				
				si palabra = Minusculas("auto") o palabra = Minusculas("automovil") entonces
					ganador = 1
				FinSi
			2:
				Escribir "le gusta saltar y es peque�o �que es?"
				leer palabra
				
				si palabra = Minusculas("saltamontes") o palabra = Minusculas("saltamonte") entonces
					ganador = 1
				FinSi
		FinSegun
		
		si ganador = 0 Entonces
			Segun adivinanza
				0:
					Escribir "suele estar en un arbol �que es?"
					leer palabra
					
					si palabra = Minusculas("manzana") entonces
						ganador = 1
					FinSi
				1:
					Escribir "lo utiliza mucha gente a dia de hoy �que es?"
					leer palabra
					
					si palabra = Minusculas("auto") o palabra = Minusculas("automovil") entonces
						ganador = 1
					FinSi
				2:
					Escribir "le gusta andar en mucha hierba y campos abiertos �que es?"
					leer palabra
					
					si palabra = Minusculas("saltamontes") o palabra = Minusculas("saltamonte") entonces
						ganador = 1
					FinSi
			FinSegun
		FinSi
		
		si ganador = 0 Entonces
			Segun adivinanza
				0:
					Escribir "tiene mucha vitamina C �que es?"
					leer palabra
					
					si palabra = Minusculas("manzana") entonces
						ganador = 1
					FinSi
				1:
					Escribir "es muy comodo y tiene ruedas �que es?"
					leer palabra
					
					si palabra = Minusculas("auto") o palabra = Minusculas("automovil") entonces
						ganador = 1
					FinSi
				2:
					Escribir "puede planear en el aire al crecer �que es?"
					leer palabra
					
					si palabra = Minusculas("saltamontes") o palabra = Minusculas("saltamonte") entonces
						ganador = 1
					FinSi
			FinSegun
		FinSi
		
		si ganador = 1 Entonces
			Borrar Pantalla
			Mientras animacion = 0 Hacer
				escribir ""
				escribir " (o.o)"
				escribir "  /|\"
				escribir "   |"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir ""
				escribir " (o.o)"
				escribir " --|--"
				escribir "   |"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir " (o.o)"
				escribir "  \|/"
				escribir "   |"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir ""
				escribir " (o.o)"
				escribir "  \|/"
				escribir "   |"
				escribir "  / \"
				animacion = 1
			Fin Mientras
			
			Escribir "Has ganado felicidades!!, �quieres volver a intentarlo?"
			escribir "escribe si o no"
			leer respuesta
		sino
			Borrar Pantalla
			Mientras animacion = 0 Hacer
				escribir ""
				escribir " (o.o)"
				escribir "  /|\"
				escribir "   |"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir ""
				escribir " (u.u)"
				escribir "   |"
				escribir "  /|\"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir ""
				escribir ""
				escribir " (u.u)"
				escribir "  /|\"
				escribir "  / \"
				
				esperar 500 milisegundos
				Borrar Pantalla
				escribir ""
				escribir ""
				escribir ""
				escribir " \\|//"
				escribir " //|\\"
				animacion = 1
			Fin Mientras
			
			Escribir "Has perdido"
			respuesta = "no"
		FinSi
	Hasta Que respuesta = "no"
FinAlgoritmo
